package com.smartdeclarers.insthota.cityweather.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utility {

    public static String convertDateTo12HRSTimeFormat(String dateStr){
        String formattedDate = "";
        try {
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("hh:mm a");
            Date date = originalFormat.parse(dateStr);
            formattedDate = targetFormat.format(date);
        } catch (ParseException e){
            e.printStackTrace();
        }
       return formattedDate;
    }

    public static String convertDateFormat(String dateStr){
        String formattedDate = "";
        try {
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("dd MMMM, YY");
            Date date = originalFormat.parse(dateStr);
            formattedDate = targetFormat.format(date);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }


}
