package com.smartdeclarers.insthota.cityweather.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.smartdeclarers.insthota.cityweather.viewmodel.ViewModelFactory;
import com.smartdeclarers.insthota.cityweather.viewmodel.WeatherDataViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(WeatherDataViewModel.class)
    abstract ViewModel bindUserViewModel(WeatherDataViewModel weatherDataViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
