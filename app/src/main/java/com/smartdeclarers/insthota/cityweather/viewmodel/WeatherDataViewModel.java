package com.smartdeclarers.insthota.cityweather.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.smartdeclarers.insthota.cityweather.model.WeatherList;
import com.smartdeclarers.insthota.cityweather.model.repositories.WeatherDataRepository;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class WeatherDataViewModel extends ViewModel {

    private MediatorLiveData<Map<String, List<WeatherList>>> weatherDataMapMediatorLiveData;
    private WeatherDataRepository weatherDataRepository;
    private final MutableLiveData<List<WeatherList>> selectedWeatherList = new MutableLiveData<>();

    public LiveData<Map<String, List<WeatherList>>> getWeatherDataObservable(){
        return weatherDataMapMediatorLiveData;
    }

    public void select(List<WeatherList> selectedList){
        selectedWeatherList.setValue(selectedList);
    }

    public LiveData<List<WeatherList>> getSelected() {
        return selectedWeatherList;
    }

    @Inject
    public WeatherDataViewModel(WeatherDataRepository weatherDataRepository){
        weatherDataMapMediatorLiveData = new MediatorLiveData<>();
        this.weatherDataRepository = weatherDataRepository;
    }

    public LiveData<Map<String, List<WeatherList>>> loadWeatherData(Map<String, String> map){
        weatherDataMapMediatorLiveData.addSource(weatherDataRepository.getCurrentCityWeatherList(map),
                weatherData -> weatherDataMapMediatorLiveData.setValue(weatherData));

        return weatherDataMapMediatorLiveData;
    }
}
