package com.smartdeclarers.insthota.cityweather.model.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.smartdeclarers.insthota.cityweather.model.WeatherForecast;
import com.smartdeclarers.insthota.cityweather.model.WeatherList;
import com.smartdeclarers.insthota.cityweather.model.service.APIService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDataRepositoryImpl implements WeatherDataRepository {

    @Inject
    APIService apiService;

    @Inject
    public WeatherDataRepositoryImpl(){}

    @Override
    public LiveData<Map<String, List<WeatherList>>> getCurrentCityWeatherList(Map<String, String> map) {
        final MutableLiveData<Map<String, List<WeatherList>>> data = new MutableLiveData<>();
        apiService.getWeatherData(map).enqueue(new Callback<WeatherForecast>() {
            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                if(response != null && response.body() != null && response.body().getCity() != null){
                    String cityName = response.body().getCity().getName();
                    List<WeatherList> weatherLists = response.body().getList();
                    Map<String, List<WeatherList>> list = new HashMap<>();
                    for(WeatherList weather : weatherLists){
                        String key = weather.getDt_txt().trim().split(" ")[0];
                        List<WeatherList> waet;
                        if(list.keySet().contains(key)){
                            waet = list.get(key);
                        } else {
                            waet = new ArrayList<>();
                        }
                        weather.setCity(cityName);
                        waet.add(weather);
                        list.put(key, waet);
                    }

                    data.setValue(list);
                } else {
                    data.setValue(null);
                }


            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}
