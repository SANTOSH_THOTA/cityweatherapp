package com.smartdeclarers.insthota.cityweather.di;

import com.smartdeclarers.insthota.cityweather.view.fragments.WeatherDetailFragment;
import com.smartdeclarers.insthota.cityweather.view.fragments.WeatherListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract WeatherListFragment contributeWeatherListFragment();

    @ContributesAndroidInjector
    abstract WeatherDetailFragment contributeWeatherDetailFragment();
}