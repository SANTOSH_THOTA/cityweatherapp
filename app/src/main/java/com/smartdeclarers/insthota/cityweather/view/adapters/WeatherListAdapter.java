package com.smartdeclarers.insthota.cityweather.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartdeclarers.insthota.cityweather.R;
import com.smartdeclarers.insthota.cityweather.model.WeatherList;
import com.smartdeclarers.insthota.cityweather.util.Utility;
import com.smartdeclarers.insthota.cityweather.viewmodel.WeatherDataViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ViewHolder>{

    private Context mCTX;
    private List<String> dateList;
    private WeatherDataViewModel weatherDataViewModel;
    Map<String, List<WeatherList>> response;

    public WeatherListAdapter(Context mCTX, WeatherDataViewModel weatherDataViewModel) {
        this.mCTX = mCTX;
        this.weatherDataViewModel = weatherDataViewModel;
        dateList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mCTX).inflate(R.layout.weather_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.dateView.setText(Utility.convertDateFormat(dateList.get(position)));
    }

    public void getWeatherList(Map<String, List<WeatherList>> response) {
        this.response = response;
        dateList.clear();
        dateList.addAll(response.keySet());
        Collections.sort(dateList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dateList.size();
    }

    public String getItem(int pos){
        return dateList.get(pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_vw)
        TextView dateView;


        public ViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    weatherDataViewModel.select(response.get(dateList.get(getAdapterPosition())));
                }
            });
        }
    }
}
