package com.smartdeclarers.insthota.cityweather.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smartdeclarers.insthota.cityweather.R;
import com.smartdeclarers.insthota.cityweather.model.WeatherList;
import com.smartdeclarers.insthota.cityweather.model.service.APIService;
import com.smartdeclarers.insthota.cityweather.util.Utility;
import com.smartdeclarers.insthota.cityweather.viewmodel.WeatherDataViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherDetailAdapter extends RecyclerView.Adapter<WeatherDetailAdapter.ViewHolder>{
    private Context mCTX;
    private WeatherDataViewModel weatherDataViewModel;
    List<WeatherList> results;

    public WeatherDetailAdapter(Context mCTX, WeatherDataViewModel weatherDataViewModel) {
        this.mCTX = mCTX;
        this.weatherDataViewModel = weatherDataViewModel;
        results = new ArrayList<>();
    }

    @Override
    public WeatherDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeatherDetailAdapter.ViewHolder(LayoutInflater.from(mCTX).inflate(R.layout.weather_detail_item, parent, false));
    }

    @Override
    public void onBindViewHolder(WeatherDetailAdapter.ViewHolder holder, int position) {
        holder.dateView.setText(Utility.convertDateTo12HRSTimeFormat(results.get(position).getDt_txt()));
        holder.description.setText(results.get(position).getWeather().get(0).getDescription());
        Glide.with(mCTX)
                .load(APIService.IMAGE_URL + results.get(position).getWeather().get(0).getIcon() + ".png")
                .into(holder.weather_icon);
    }

    public void getWeatherList(List<WeatherList> results) {
        this.results = results;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time_tv)
        TextView dateView;

        @BindView(R.id.desc_tv)
        TextView description;

        @BindView(R.id.icon_iv)
        ImageView weather_icon;


        public ViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(mCTX, "Clicked Position- " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
