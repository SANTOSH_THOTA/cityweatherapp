package com.smartdeclarers.insthota.cityweather.view.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.smartdeclarers.insthota.cityweather.R;
import com.smartdeclarers.insthota.cityweather.view.fragments.WeatherDetailFragment;
import com.smartdeclarers.insthota.cityweather.view.fragments.WeatherListFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;


public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        doMainActivityFunction();
    }

    private void doMainActivityFunction(){
        WeatherListFragment weatherListFragment = new WeatherListFragment();
        WeatherDetailFragment weatherDetailFragment = new WeatherDetailFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.bottom_container, weatherListFragment).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.top_container, weatherDetailFragment).commit();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
