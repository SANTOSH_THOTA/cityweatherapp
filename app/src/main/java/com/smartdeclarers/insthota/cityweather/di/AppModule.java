package com.smartdeclarers.insthota.cityweather.di;

import com.smartdeclarers.insthota.cityweather.model.repositories.WeatherDataRepository;
import com.smartdeclarers.insthota.cityweather.model.repositories.WeatherDataRepositoryImpl;
import com.smartdeclarers.insthota.cityweather.model.service.APIService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {

    @Singleton
    @Provides
    APIService provideAPIService() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(APIService.BASE_URL)
                .build()
                .create(APIService.class);
    }

    @Singleton
    @Provides
    WeatherDataRepository provideWeatherDataRepository(WeatherDataRepositoryImpl weatherDataRepository){
        return weatherDataRepository;
    }
}
