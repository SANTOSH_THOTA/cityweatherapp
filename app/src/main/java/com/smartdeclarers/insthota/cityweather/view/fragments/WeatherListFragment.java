package com.smartdeclarers.insthota.cityweather.view.fragments;

import android.Manifest;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.smartdeclarers.insthota.cityweather.BuildConfig;
import com.smartdeclarers.insthota.cityweather.R;
import com.smartdeclarers.insthota.cityweather.di.Injectable;
import com.smartdeclarers.insthota.cityweather.model.service.APIService;
import com.smartdeclarers.insthota.cityweather.view.adapters.WeatherListAdapter;
import com.smartdeclarers.insthota.cityweather.viewmodel.WeatherDataViewModel;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WeatherListFragment extends Fragment implements Injectable {
    private static final String TAG = WeatherListFragment.class.getSimpleName();

    private WeatherDataViewModel weatherDataViewModel;
    private View view;

    @BindView(R.id.rv_data)
    RecyclerView recyclerView;

    @BindView(R.id.city_ed)
    EditText city_ed;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private WeatherListAdapter adapter;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_weather_list, container, false);
        ButterKnife.bind(this,view);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        return view;
    }

    private void updateUI(){
        recyclerView.setHasFixedSize(true);
        adapter = new WeatherListAdapter(getActivity(), weatherDataViewModel);
        recyclerView.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),1);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @OnClick(R.id.city_btn)
    protected void getCityWeather(){
        doRequest(city_ed.getText().toString());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        weatherDataViewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                .get(WeatherDataViewModel.class);

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }

        updateUI();
    }

    private void doRequest(String city){
        Map<String, String> map = new HashMap<>();
        map.put("q", city);
        map.put("mode", "json");
        map.put("appid", APIService.API_KEY);
        weatherDataViewModel.loadWeatherData(map);

        weatherDataViewModel.getWeatherDataObservable().observe(this, weatherAPIResponse -> {
            if (weatherAPIResponse == null) {
                Toast.makeText(getContext(), "Something went worng, Please check city name is proper or not. Try again", Toast.LENGTH_LONG).show();
            } else {
                adapter.getWeatherList(weatherAPIResponse);
                weatherDataViewModel.select(weatherAPIResponse.get(adapter.getItem(0)));
            }
        });
    }

    private void doRequest(double lat, double lon){
        Map<String, String> map = new HashMap<>();
        map.put("lat", lat+"");
        map.put("lon", lon+"");
        map.put("mode", "json");
        map.put("appid", APIService.API_KEY);
        weatherDataViewModel.loadWeatherData(map);

        weatherDataViewModel.getWeatherDataObservable().observe(this, weatherAPIResponse -> {
            if (weatherAPIResponse == null) {
                Toast.makeText(getContext(), "Something went worng, Please check city name is proper or not. Try again", Toast.LENGTH_LONG).show();
            } else {
                adapter.getWeatherList(weatherAPIResponse);
                weatherDataViewModel.select(weatherAPIResponse.get(adapter.getItem(0)));
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            doRequest(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    private void showSnackbar(final String text) {
        View container = getActivity().findViewById(R.id.main_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }
}
