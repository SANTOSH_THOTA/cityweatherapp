package com.smartdeclarers.insthota.cityweather.model.service;

import com.smartdeclarers.insthota.cityweather.model.WeatherForecast;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface APIService {
    public static final String IMAGE_URL = "http://openweathermap.org/img/w/";
    public static final String BASE_URL = "https://api.openweathermap.org/";
    public static final String API_KEY = "6be5c716ceb65e198a85fff22cd4acdd";

    @GET("/data/2.5/forecast")
    Call<WeatherForecast> getWeatherData(@QueryMap Map<String, String> params);
}
