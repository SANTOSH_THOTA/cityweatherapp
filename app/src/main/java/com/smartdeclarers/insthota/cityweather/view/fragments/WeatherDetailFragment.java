package com.smartdeclarers.insthota.cityweather.view.fragments;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smartdeclarers.insthota.cityweather.R;
import com.smartdeclarers.insthota.cityweather.di.Injectable;
import com.smartdeclarers.insthota.cityweather.model.WeatherList;
import com.smartdeclarers.insthota.cityweather.util.Utility;
import com.smartdeclarers.insthota.cityweather.view.adapters.WeatherDetailAdapter;
import com.smartdeclarers.insthota.cityweather.viewmodel.WeatherDataViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherDetailFragment extends Fragment implements Injectable {

    private WeatherDataViewModel weatherDataViewModel;
    private View view;

    private WeatherDetailAdapter weatherDetailAdapter;

    @BindView(R.id.rv_data)
    RecyclerView recyclerView;

    @BindView(R.id.header_id)
    TextView headerText;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        weatherDataViewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                .get(WeatherDataViewModel.class);

        weatherDetailAdapter = new WeatherDetailAdapter(getActivity(), weatherDataViewModel);
        recyclerView.setAdapter(weatherDetailAdapter);

        weatherDataViewModel.getSelected().observe(this, weatherDetailResponse -> {
            if (weatherDetailResponse == null) {
                Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
            } else {
                updateUI(weatherDetailResponse);
            }
        });

    }


    private void updateUI(List<WeatherList> weatherDetailResponse){
        headerText.setVisibility(View.VISIBLE);
        headerText.setText("Weather Details \nCity: " +weatherDetailResponse.get(0).getCity() + " \nDate: " + Utility.convertDateFormat(weatherDetailResponse.get(0).getDt_txt().split(" ")[0]));

        weatherDetailAdapter.getWeatherList(weatherDetailResponse);
    }

}
