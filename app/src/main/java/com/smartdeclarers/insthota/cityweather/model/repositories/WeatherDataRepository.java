package com.smartdeclarers.insthota.cityweather.model.repositories;

import android.arch.lifecycle.LiveData;

import com.smartdeclarers.insthota.cityweather.model.WeatherList;

import java.util.List;
import java.util.Map;

public interface WeatherDataRepository {
    public LiveData<Map<String, List<WeatherList>>> getCurrentCityWeatherList(Map<String, String> map);
}
